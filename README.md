Jogo feito para a disciplina de Org. de Computadores Digitais - Prof. Eduardo Simões
 ---------------------------------------
  Implementação para o ICMC Processor 
   do jogo de atari de 1976           
 ---------------------------------------

 Controles:
  - Jogador 1
    - W - Cima
    - A - Esquerda
    - S - Baixo
    - D - Direita
    - SPACE - atira

  - Jogador 2
    - I - Cima
    - J - Esquerda
    - K - Baixo
    - L - Direita
    - ENTER - atira

 Grupo:
 - Geraldo Murilo Carrijo Viana Alves da Silva
 - Iara Duarte Mainates
 - Lucas Caetano Procópio

Prévia do nosso jogo: https://drive.google.com/file/d/1Gzfpj624OaMEgNRbq27U2Bet5SeoJEnJ/view?usp=sharing

Não conhece o jogo? Saiba mais aqui: https://en.wikipedia.org/wiki/Outlaw_(video_game) e aqui https://www.youtube.com/watch?v=JulZ3c6ChW4&ab_channel=Highretrogamelord

Dúvidas é só mandar mensagem! :)
