;   ___       _   _             
;  / _ \ _  _| |_| |__ ___ __ __
; | (_) | || |  _| / _` \ V  V /
;  \___/ \_,_|\__|_\__,_|\_/\_/ 
; ---------------------------------------
; | Implementação para o ICMC Processor |
; |  do jogo de atari de 1976           |
; ---------------------------------------
; Grupo:
; - Geraldo Murilo Carrijo Viana Alves da Silva
; - Iara Duarte Mainates
; - Lucas Caetano Procópio

reset:
    loadn r0, #0
    store player_pontuacao, r0
    store enemy_pontuacao, r0
    loadn r0, #1035 ;00000100 00001011
    store player_pos, r0
    loadn r0, #7435 ;00011101 00001011
    store enemy_pos, r0
    
    loadn r0, #0
    store character_walking, r0
    store character_shooting, r0
    store enemy_walking, r0
    store enemy_shooting, r0
    
imprime_tela_intro_1:
    loadn r0, #tela_intro_1
    loadn r1, #0
    loadn r2, #1200
imprime_tela_intro_1_loop:
    loadi r3, r0
    outchar r3, r1
    inc r0
    inc r1
    cmp r1, r2
    jne imprime_tela_intro_1_loop
    
    loadn r1, #255
aguarda_inicio:
    inchar r0
    cmp r0, r1
    jeq aguarda_inicio

game_loop:
; O game loop:
;   - Receber input
;   - Matemática/Física
;       \- Ao mesmo tempo popular o frame buffer
;   - Imprimir o frame
;   - Repetir até o fim do jogo
limpa_buffer:
    loadn r0, #0
    loadn r1, #1200
    loadn r2, #frame_buffer
    loadn r3, #32
limpa_buffer_loop:
    storei r2, r3
    inc r2
    inc r0
    cmp r0, r1
    jne limpa_buffer_loop

recebe_input:
    inchar r0

move_personagem:
    loadn r1, #100
    cmp r0, r1
    ceq incrementa_x
    
    loadn r1, #119
    cmp r0, r1
    ceq decrementa_y
    
    loadn r1, #97
    cmp r0, r1
    ceq decrementa_x
    
    loadn r1, #115
    cmp r0, r1
    ceq incrementa_y
    
    loadn r1, #32
    cmp r0, r1
    ceq atira
exit_move_personagem:
    ; ENEMY
    loadn r1, #108
    cmp r0, r1
    ceq enemy_incrementa_x
    
    loadn r1, #105
    cmp r0, r1
    ceq enemy_decrementa_y
    
    loadn r1, #106
    cmp r0, r1
    ceq enemy_decrementa_x
    
    loadn r1, #107
    cmp r0, r1
    ceq enemy_incrementa_y
    
    loadn r1, #13
    cmp r0, r1
    ceq enemy_atira

    load r0, player_bullet_pos
    shiftr0 r0, #8
    loadn r1, #39
    cmp r0, r1
    jgr exit_move_bullet
    
    load r1, enemy_pos
    shiftr0 r1, #8
    cmp r0, r1
    jle exit_move_bullet
    loadn r2, #8
    add r1, r1, r2
    cmp r0, r1
    jeg exit_move_bullet
    load r0, player_bullet_pos
    loadn r1, #255
    and r0, r0, r1
    load r1, enemy_pos
    loadn r2, #255
    and r1, r1, r2
    cmp r0, r1
    jle exit_move_bullet
    loadn r2, #10
    add r1, r1, r2
    cmp r0, r1
    jeg exit_move_bullet
    
    load r0, player_pontuacao
    inc r0
    store player_pontuacao, r0
    loadn r0, #65280
    store player_bullet_pos, r0
    
exit_move_bullet:

    load r0, enemy_bullet_pos
    shiftr0 r0, #8
    loadn r1, #1
    cmp r0, r1
    jle enemy_exit_move_bullet
    
    load r1, player_pos
    shiftr0 r1, #8
    cmp r0, r1
    jle enemy_exit_move_bullet
    loadn r2, #8
    add r1, r1, r2
    cmp r0, r1
    jeg enemy_exit_move_bullet
    load r0, enemy_bullet_pos
    loadn r1, #255
    and r0, r0, r1
    load r1, player_pos
    loadn r2, #255
    and r1, r1, r2
    cmp r0, r1
    jle enemy_exit_move_bullet
    loadn r2, #10
    add r1, r1, r2
    cmp r0, r1
    jeg enemy_exit_move_bullet
    load r0, enemy_pontuacao
    inc r0
    store enemy_pontuacao, r0
    loadn r0, #0
    store enemy_bullet_pos, r0
    
enemy_exit_move_bullet:

    load r0, character_shooting
    loadn r1, #0
    cmp r0, r1
    loadn r0, #spr_personagem_atirando
    store curr_spr_addr, r0
    jgr exit_character_walk
    
    load r0, character_walking
    loadn r1, #5
    cmp r0, r1
    jeg character_walk_2
character_walk_1:
    loadn r0, #spr_personagem_andando_1
    store curr_spr_addr, r0
    jmp exit_character_walk
character_walk_2:
    loadn r0, #spr_personagem_andando_2
    store curr_spr_addr, r0
    jmp exit_character_walk
exit_character_walk:

    loadn r0, #10508 ; 0 01010 01000 0010 0
    store curr_spr_data, r0
    
    load r0, player_pos
    store curr_spr_pos, r0
    
    call desenha
    
    load r0, player_pontuacao
    
    loadn r1, #0
    cmp r0, r1
    jeq pontuacao_valor_0
    
    loadn r1, #1
    cmp r0, r1
    jeq pontuacao_valor_1
    
    loadn r1, #2
    cmp r0, r1
    jeq pontuacao_valor_2
    
    loadn r1, #3
    cmp r0, r1
    jeq pontuacao_valor_3
    
    loadn r1, #4
    cmp r0, r1
    jeq pontuacao_valor_4
    
    loadn r1, #5
    cmp r0, r1
    jeq pontuacao_valor_5
    
    loadn r1, #6
    cmp r0, r1
    jeq pontuacao_valor_6
    
    loadn r1, #7
    cmp r0, r1
    jeq pontuacao_valor_7
    
    loadn r1, #8
    cmp r0, r1
    jeq pontuacao_valor_8
    
    loadn r1, #9
    cmp r0, r1
    jeq pontuacao_valor_9
    
    loadn r1, #10
    cmp r0, r1
    jeq fim_jogador_1
pontuacao_valor_0:
    loadn r0, #spr_num_0
    jmp draw_pontuacao
pontuacao_valor_1:
    loadn r0, #spr_num_1
    jmp draw_pontuacao
pontuacao_valor_2:
    loadn r0, #spr_num_2
    jmp draw_pontuacao
pontuacao_valor_3:
    loadn r0, #spr_num_3
    jmp draw_pontuacao
pontuacao_valor_4:
    loadn r0, #spr_num_4
    jmp draw_pontuacao
pontuacao_valor_5:
    loadn r0, #spr_num_5
    jmp draw_pontuacao
pontuacao_valor_6:
    loadn r0, #spr_num_6
    jmp draw_pontuacao
pontuacao_valor_7:
    loadn r0, #spr_num_7
    jmp draw_pontuacao
pontuacao_valor_8:
    loadn r0, #spr_num_8
    jmp draw_pontuacao
pontuacao_valor_9:
    loadn r0, #spr_num_9
    jmp draw_pontuacao
    
draw_pontuacao:
    store curr_spr_addr, r0
    
    loadn r0, #5216 ; 0 00101 01000 0000 0
    store curr_spr_data, r0
    
    loadn r0, #2049 ; 00001000 00000001
    store curr_spr_pos, r0
    
    call desenha
    
    ; ADVANCE TIMERS
draw_bullet:
    load r0, player_bullet_pos
    shiftr0 r0, #8
    
    loadn r1, #1
    sub r0, r0, r1
    
    loadn r1, #0
    cmp r0, r1
    jeq exit_draw_bullet
    loadn r1, #39
    cmp r0, r1
    jeg exit_draw_bullet
    
    load r1, player_bullet_pos
    loadn r2, #255
    and r1, r1, r2
    loadn r2, #40
    mul r1, r1, r2
    add r0, r0, r1
    loadn r1, #'o'
    loadn r2, #frame_buffer
    add r0, r0, r2
    storei r0, r1
    
exit_draw_bullet:

enemy_draw_bullet:
    load r0, enemy_bullet_pos
    shiftr0 r0, #8
    
    loadn r1, #1
    cmp r0, r1
    jel enemy_exit_draw_bullet
    load r1, enemy_bullet_pos
    loadn r2, #255
    and r1, r1, r2
    loadn r2, #40
    mul r1, r1, r2
    add r0, r0, r1
    loadn r1, #'o'
    loadn r2, #frame_buffer
    add r0, r0, r2
    storei r0, r1
    
enemy_exit_draw_bullet:

; enemy
    load r0, enemy_shooting
    loadn r1, #0
    cmp r0, r1
    jgr exit_move_enemy

    loadn r0, #0
    store enemy_bullet_pos, r0
move_enemy:

exit_move_enemy:
    load r0, enemy_shooting
    loadn r1, #0
    cmp r0, r1
    loadn r0, #spr_personagem_atirando
    store curr_spr_addr, r0
    jgr exit_enemy_walk
    
    load r0, enemy_walking
    loadn r1, #5
    cmp r0, r1
    jeg enemy_walk_2
enemy_walk_1:
    loadn r0, #spr_personagem_andando_1
    store curr_spr_addr, r0
    jmp exit_enemy_walk
enemy_walk_2:
    loadn r0, #spr_personagem_andando_2
    store curr_spr_addr, r0
    jmp exit_enemy_walk
exit_enemy_walk:

    loadn r0, #10499 ; 01010 01000 0000 1
    store curr_spr_data, r0
    
    load r0, enemy_pos
    store curr_spr_pos, r0
    
    call desenha

    load r0, enemy_pontuacao
    
    loadn r1, #0
    cmp r0, r1
    jeq enemy_pontuacao_valor_0
    
    loadn r1, #1
    cmp r0, r1
    jeq enemy_pontuacao_valor_1
    
    loadn r1, #2
    cmp r0, r1
    jeq enemy_pontuacao_valor_2
    
    loadn r1, #3
    cmp r0, r1
    jeq enemy_pontuacao_valor_3
    
    loadn r1, #4
    cmp r0, r1
    jeq enemy_pontuacao_valor_4
    
    loadn r1, #5
    cmp r0, r1
    jeq enemy_pontuacao_valor_5
    
    loadn r1, #6
    cmp r0, r1
    jeq enemy_pontuacao_valor_6
    
    loadn r1, #7
    cmp r0, r1
    jeq enemy_pontuacao_valor_7
    
    loadn r1, #8
    cmp r0, r1
    jeq enemy_pontuacao_valor_8
    
    loadn r1, #9
    cmp r0, r1
    jeq enemy_pontuacao_valor_9
    
    loadn r1, #10
    cmp r0, r1
    jeq fim_jogador_2
enemy_pontuacao_valor_0:
    loadn r0, #spr_num_0
    jmp enemy_draw_pontuacao
enemy_pontuacao_valor_1:
    loadn r0, #spr_num_1
    jmp enemy_draw_pontuacao
enemy_pontuacao_valor_2:
    loadn r0, #spr_num_2
    jmp enemy_draw_pontuacao
enemy_pontuacao_valor_3:
    loadn r0, #spr_num_3
    jmp enemy_draw_pontuacao
enemy_pontuacao_valor_4:
    loadn r0, #spr_num_4
    jmp enemy_draw_pontuacao
enemy_pontuacao_valor_5:
    loadn r0, #spr_num_5
    jmp enemy_draw_pontuacao
enemy_pontuacao_valor_6:
    loadn r0, #spr_num_6
    jmp enemy_draw_pontuacao
enemy_pontuacao_valor_7:
    loadn r0, #spr_num_7
    jmp enemy_draw_pontuacao
enemy_pontuacao_valor_8:
    loadn r0, #spr_num_8
    jmp enemy_draw_pontuacao
enemy_pontuacao_valor_9:
    loadn r0, #spr_num_9
    jmp enemy_draw_pontuacao
    
enemy_draw_pontuacao:
    store curr_spr_addr, r0
    
    loadn r0, #5216 ; 0 00101 01000 0000 0
    store curr_spr_data, r0
    
    loadn r0, #7425; 00011101 00000001
    store curr_spr_pos, r0
    
    call desenha

advance_character_shoot:
    load r0, character_shooting
    loadn r1, #0
    cmp r0, r1
    
    jeq exit_advance_character_shoot
    dec r0
    store character_shooting, r0
exit_advance_character_shoot:

advance_enemy_shoot:
    load r0, enemy_shooting
    loadn r1, #0
    cmp r0, r1
    
    jeq exit_advance_enemy_shoot
    dec r0
    store enemy_shooting, r0
exit_advance_enemy_shoot:

advance_character_bullet:
    load r0, player_bullet_timer
    dec r0
    store player_bullet_timer, r0
    loadn r1, #0
    cmp r0, r1
    
    jne exit_advance_character_bullet
    loadn r0, #1
    store player_bullet_timer, r0
    
    load r0, player_bullet_pos
    shiftr0 r0, #8
    loadn r1, #39
    cmp r0, r1
    jgr exit_advance_character_bullet
    load r0, player_bullet_pos
    loadn r1, #256
    add r0, r0, r1
    store player_bullet_pos, r0
exit_advance_character_bullet:

advance_enemy_bullet:
    load r0, enemy_bullet_timer
    dec r0
    store enemy_bullet_timer, r0
    loadn r1, #0
    cmp r0, r1
    
    jne exit_advance_enemy_bullet
    loadn r0, #1
    store enemy_bullet_timer, r0
    
    load r0, enemy_bullet_pos
    shiftr0 r0, #8
    loadn r1, #1
    cmp r0, r1
    ;jeg exit_advance_enemy_bullet
    load r0, enemy_bullet_pos
    loadn r1, #256
    sub r0, r0, r1
    store enemy_bullet_pos, r0
exit_advance_enemy_bullet:

    call imprime_tela
    jmp game_loop

incrementa_x:
    push r0
    push r1
    push r2
    
    load r0, character_walking
    inc r0
    store character_walking, r0
    loadn r1, #9
    cmp r0, r1
    jne exit_walk_inc_x
    loadn r0, #0
    store character_walking, r0
exit_walk_inc_x:

    load r0, player_pos
    loadn r1, #256 ; 00000001 00000000
    add r0, r0, r1
    
    loadn r2, #65280
    and r1, r0, r2
    shiftr0 r1, #8
    loadn r2, #13
    cmp r1, r2
    jeq exit_incrementa_x
    store player_pos, r0
exit_incrementa_x:
    pop r2
    pop r1
    pop r0
    rts

incrementa_y:
    push r0
    push r1
    push r2

    load r0, character_walking
    inc r0
    store character_walking, r0
    loadn r1, #9
    cmp r0, r1
    jne exit_walk_inc_y
    loadn r0, #0
    store character_walking, r0
exit_walk_inc_y:

    load r0, player_pos
    loadn r1, #1 ; 00000000 00000001
    add r0, r0, r1
    
    loadn r2, #255
    and r1, r0, r2
    loadn r2, #21
    cmp r1, r2
    jeq exit_incrementa_y
    store player_pos, r0
exit_incrementa_y:
    pop r2
    pop r1
    pop r0
    rts
    
decrementa_x:
    push r0
    push r1
    push r2

    load r0, character_walking
    inc r0
    store character_walking, r0
    loadn r1, #9
    cmp r0, r1
    jne exit_walk_dec_x
    loadn r0, #0
    store character_walking, r0
exit_walk_dec_x:
  
    load r0, player_pos
    
    loadn r2, #65280
    and r1, r0, r2
    shiftr0 r1, #8
    loadn r2, #0
    cmp r1, r2
    jeq exit_decrementa_x
    loadn r1, #256 ; 00000001 00000000
    sub r0, r0, r1
    store player_pos, r0
exit_decrementa_x:
    pop r2
    pop r1
    pop r0
    rts

decrementa_y:
    push r0
    push r1
    push r2

    load r0, character_walking
    inc r0
    store character_walking, r0
    loadn r1, #9
    cmp r0, r1
    jne exit_walk_dec_y
    loadn r0, #0
    store character_walking, r0
exit_walk_dec_y:
    
    load r0, player_pos
    
    loadn r2, #255
    and r1, r0, r2
    loadn r2, #7
    cmp r1, r2
    jeq exit_decrementa_y
    
    loadn r1, #1 ; 00000000 00000001
    sub r0, r0, r1
    store player_pos, r0
exit_decrementa_y:
    pop r2
    pop r1
    pop r0
    rts

atira:
    push r0
    push r1
    push r2
    
    load r0, character_shooting
    loadn r1, #0
    cmp r0, r1
    jgr exit_atira
    
    loadn r0, #50
    store character_shooting, r0
    
    ;playerpos + (8, 4)
    ;enemypos - (1, 4)
    load r0, player_pos
    loadn r1, #8
    shiftl0 r1, #8
    add r0, r0, r1
    loadn r1, #4
    add r0, r0, r1
    store player_bullet_pos, r0
    loadn r0, #2
    store player_bullet_timer, r0
    
exit_atira:
    pop r2
    pop r1
    pop r0
    rts

enemy_incrementa_x:
    push r0
    push r1
    push r2
    
    load r0, enemy_walking
    inc r0
    store enemy_walking, r0
    loadn r1, #9
    cmp r0, r1
    jne enemy_exit_walk_inc_x
    loadn r0, #0
    store enemy_walking, r0
enemy_exit_walk_inc_x:

    load r0, enemy_pos
    loadn r1, #256 ; 00000001 00000000
    add r0, r0, r1
    
    loadn r2, #65280
    and r1, r0, r2
    shiftr0 r1, #8
    loadn r2, #33
    cmp r1, r2
    jeq enemy_exit_enemy_incrementa_x
    store enemy_pos, r0
enemy_exit_enemy_incrementa_x:
    pop r2
    pop r1
    pop r0
    rts

enemy_incrementa_y:
    push r0
    push r1
    push r2

    load r0, enemy_walking
    inc r0
    store enemy_walking, r0
    loadn r1, #9
    cmp r0, r1
    jne enemy_exit_walk_inc_y
    loadn r0, #0
    store enemy_walking, r0
enemy_exit_walk_inc_y:

    load r0, enemy_pos
    loadn r1, #1 ; 00000000 00000001
    add r0, r0, r1
    
    loadn r2, #255
    and r1, r0, r2
    loadn r2, #21
    cmp r1, r2
    jeq enemy_exit_enemy_incrementa_y
    store enemy_pos, r0
enemy_exit_enemy_incrementa_y:
    pop r2
    pop r1
    pop r0
    rts
    
enemy_decrementa_x:
    push r0
    push r1
    push r2

    load r0, enemy_walking
    inc r0
    store enemy_walking, r0
    loadn r1, #9
    cmp r0, r1
    jne enemy_exit_walk_dec_x
    loadn r0, #0
    store enemy_walking, r0
enemy_exit_walk_dec_x:
  
    load r0, enemy_pos
    loadn r2, #65280
    and r1, r0, r2
    shiftr0 r1, #8
    loadn r2, #20
    cmp r1, r2
    jeq enemy_exit_decrementa_x
    loadn r1, #256 ; 00000001 00000000
    sub r0, r0, r1
    store enemy_pos, r0
enemy_exit_decrementa_x:
    pop r2
    pop r1
    pop r0
    rts

enemy_decrementa_y:
    push r0
    push r1
    push r2

    load r0, enemy_walking
    inc r0
    store enemy_walking, r0
    loadn r1, #9
    cmp r0, r1
    jne enemy_exit_walk_dec_y
    loadn r0, #0
    store enemy_walking, r0
enemy_exit_walk_dec_y:
    
    load r0, enemy_pos
    
    loadn r2, #255
    and r1, r0, r2
    loadn r2, #7
    cmp r1, r2
    jeq enemy_exit_decrementa_y
    
    loadn r1, #1 ; 00000000 00000001
    sub r0, r0, r1
    store enemy_pos, r0
enemy_exit_decrementa_y:
    pop r2
    pop r1
    pop r0
    rts

enemy_atira:
    push r0
    push r1
    push r2
    
    load r0, enemy_shooting
    loadn r1, #0
    cmp r0, r1
    jgr enemy_exit_atira
    
    loadn r0, #50
    store enemy_shooting, r0
    
    ;playerpos + (8, 4)
    ;enemypos - (1, 4)
    load r0, enemy_pos
    loadn r1, #256
    sub r0, r0, r1
    loadn r1, #4
    add r0, r0, r1
    store enemy_bullet_pos, r0
    loadn r0, #2
    store enemy_bullet_timer, r0
    
enemy_exit_atira:
    pop r2
    pop r1
    pop r0
    rts

fim_jogador_1:
    loadn r0, #tela_fim_1
    loadn r1, #0
    loadn r2, #1200
fim_jogador_1_loop:
    loadi r3, r0
    outchar r3, r1
    inc r0
    inc r1
    cmp r1, r2
    jne fim_jogador_1_loop
    loadn r1, #255
fim_1_aguarda_inicio:
    inchar r0
    cmp r0, r1
    jeq fim_1_aguarda_inicio
    jmp reset
    
fim_jogador_2:
    loadn r0, #tela_fim_2
    loadn r1, #0
    loadn r2, #1200
fim_jogador_2_loop:
    loadi r3, r0
    outchar r3, r1
    inc r0
    inc r1
    cmp r1, r2
    jne fim_jogador_1_loop
    loadn r1, #255
fim_2_aguarda_inicio:
    inchar r0
    cmp r0, r1
    jeq fim_2_aguarda_inicio
    jmp reset

; +------------------------------------------------------------+
; | Desenha o sprite descrito em curr_spr_addr, curr_spr_pos e |
; | curr_spr_data. Executado uma vez por sprite.               |
; +------------------------------------------------------------+
; Posiciona o sprite descrito em curr_sprite_addr, curr_spr_pos
; e curr_spr_data no frame buffer.
desenha:
    push r0
    push r1
    push r2
    push r3
    push r4
posiciona_cursor:
    ; A posição inicial do cursor é (Y * 40) + X
    load r0, curr_spr_pos
    loadn r1, #255 ; 00000000 11111111, mascara a posição Y
    and r0, r0, r1
    
    loadn r1, #40
    mul r0, r0, r1
    
    load r1, curr_spr_pos
    loadn r2, #65280 ; 11111111 00000000, mascara a posição X
    and r1, r1, r2
    shiftr0 r1, #8
    
    add r0, r0, r1
    
    ; Se o sprite estiver invertido (bit 0 em curr_spr_data), devemos
    ; imprimir as linhas ao contrário, então a posição do cursor passa
    ; a ser a atual + largura, e seu valor é decrementado ao imprimir.
    load r1, curr_spr_data
    loadn r2, #1 ; 0 00000 00000 0000 1
    and r1, r1, r2
    cmp r1, r2
    jne desenha_sprite
    
    load r1, curr_spr_data
    loadn r2, #992 ; 0 00000 11111 0000 0
    and r1, r1, r2
    shiftr0 r1, #5
    loadn r2, #1
    sub r1, r1, r2 ; como a linha começa em 0, subtrai 1
    add r0, r0, r1
desenha_sprite:
    load r1, curr_spr_addr
desenha_sprite_loop:
    loadi r2, r1
    
    ; Verifica se o sprite acabou
    loadn r3, #'\0'
    cmp r2, r3
    jeq desenha_saida
    
    ; Se não, imprime o caractere e avança o cursor
    loadn r3, #frame_buffer
    add r3, r3, r0
    
    load r4, curr_spr_data
    loadn r5, #30 ;00000 00000 1111 0
    and r4, r4, r5
    shiftl0 r4, #7
    add r2, r2, r4
    
    storei r3, r2
    
    inc r1 ; proximo char
avanca_cursor:
    push r0
    push r1
    
    load r1, curr_spr_pos
    loadn r2, #65280 ; 11111111 00000000, mascara a posição X
    and r1, r1, r2
    shiftr0 r1, #8
    
    loadn r2, #40
    mod r0, r0, r2
    
    sub r0, r0, r1
    ; r0 agora é a posx
    
    ; verifica se o sprite está invertido
    load r1, curr_spr_data
    loadn r2, #1
    and r1, r1, r2
    cmp r1, r2
    
    load r1, curr_spr_data
    loadn r2, #992 ; 000001111100000
    and r1, r1, r2
    shiftr0 r1, #5
    loadn r2, #1
    sub r1, r1, r2
    ; r1 agora é a largura (menos 1 pq a posição começa em 0)
    jeq avanca_cursor_invertido
    
    loadn r2, #39
    sub r2, r2, r1
    ;r2 é a quantidade a ser incrementada para pular para a linha de baixo
    
    cmp r0, r1
    pop r1
    pop r0
    jle incrementa_cursor ; quebra a linha
    add r0, r0, r2
incrementa_cursor:
    inc r0
    jmp desenha_sprite_loop
avanca_cursor_invertido:
    loadn r2, #0
    cmp r0, r2
    ;se o valor for 0, desce para a linha de baixo
    
    loadn r2, #41
    add r2, r2, r1
    ;r2 é a quantidade a ser incrementada para pular para a linha de baixo
    
    pop r1
    pop r0
    jgr decrementa_cursor
    add r0, r0, r2
decrementa_cursor:
    dec r0
    jmp desenha_sprite_loop
desenha_saida:
    pop r4
    pop r3
    pop r2
    pop r1
    pop r0
    rts

imprime_tela:
    loadn r0, #frame_buffer
    loadn r1, #1200
    loadn r2, #0
imprime_tela_loop:
    loadi r3, r0
    outchar r3, r2
    inc r0
    inc r2
    cmp r2, r1
    jne imprime_tela_loop
    rts
    
; +===========================================+
; | Inicialização de umas coisas e reservando |
; | espaço na memória para outras             |
; +===========================================+

frame_buffer: var #1200

curr_spr_addr: var #1   ; Endereço do sprite atual.

curr_spr_pos: var #1    ; Most significant byte guarda a posição em X,
                        ; least significant a posição em Y.

curr_spr_data: var #1   ; 00000 00000 0000 0
                        ; ||||| ||||| |||| +- Bit 0: Invertido?
                        ; ||||| ||||| ++++--- Bits 1~4: Cor
                        ; ||||| +++++-------- Bits 5~9: Largura
                        ; +++++-------------- Bits 10~14: Altura
                        
player_pos: var #1
player_bullet_pos: var #1
player_bullet_timer: var #1

enemy_pos: var #1
enemy_bullet_pos: var #1
enemy_bullet_timer: var #1

player_pontuacao: var #1
enemy_pontuacao: var #1

character_walking: var #1
character_shooting: var #1
enemy_walking: var #1
enemy_shooting: var #1
; 8 x 10
spr_personagem_andando_1: string "   ##     #####    ###     ##    ###### #  ##  # # ## #   ####    #  #    ## ## "
spr_personagem_andando_2: string "   ##     #####    ###     ##    ###### #  ##  # # ## #   ####   ##  ## ##    ##"
spr_personagem_atirando: string  "         ##     #####    ###     ##  ### #####   ##      ####     # #   ### ##  "
spr_personagem_morto: string     "                           #     #####     #       #     #####   #####   #####  "

; 3 x 5
spr_num_0: string "0000 00 00 0000"
spr_num_1: string "  0  0  0  0  0"
spr_num_2: string "000  00000  000"
spr_num_3: string "000  0 00  0000"
spr_num_4: string "0 00 0000  0  0"
spr_num_5: string "0000  000  0000"
spr_num_6: string "0000  0000 0000"
spr_num_7: string "0000 0  0  0  0"
spr_num_8: string "0000 00000 0000"
spr_num_9: string "0000 0000  0  0"

; Telas
tela_intro_1: string "                                               ___       _   _                        / _ \\ _  _| |_| |__ ___ __ __          | (_) | || |  _| / _` \\ V  V /           \\___/ \\_,_|\\__|_\\__,_|\\_/\\_/                                              ========================================                                          Implementacao para o ICMC Processor     do jogo para atari de 1976.                                                     Grupo:                                     - Geraldo Murilo C V A Silva            - Iara Duarte Mainates                  - Lucas Caetano Procopio                                                                                                                                                                                                                     Pressione qualquer tecla para jogar!                                                                                                                                                                                                                                                                                                                                                                          "
tela_fim_1:   string "                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           Jogador 1 venceu!              Aperte qualquer tecla para recomecar                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  "
tela_fim_2:   string "                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           Jogador 2 venceu!              Aperte qualquer tecla para recomecar                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  "
